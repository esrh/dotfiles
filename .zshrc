# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/eshanrh/.oh-my-zsh"
ZSH_THEME="theunraveler"



source $ZSH/oh-my-zsh.sh

alias jrnl=" jrnl"
alias ssh="TERM=xterm-256color ssh"
alias switchdisplays="xrandr --output eDP --auto && xrandr --output HDMI-A-0 --off && xrandr --output eDP --mode 1280x720"
alias dotfiles="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias disablelaptopscreen="xrandr --output HDMI-A-0 --primary && xrandr --output eDP --off"
alias enablelaptopscreen="xrandr --output eDP --auto && xrandr --output eDP --left-of HDMI-A-0"
alias lock="sh $HOME/.i3lock-script.sh"
alias uninstallorphans="sudo pacman -Rns $(pacman -Qtdq)"
alias osu="wine $HOME/games/osu/osu\!.exe"
alias doom="~/.emacs.d/bin/doom"

emcs(){
    emacsclient $1 &
    disown
}
startjpynb(){
    cd $1
    nohup jupyter notebook --allow-root >.jpyerrors & echo $!>~/.jpypid
}
killjpynb(){
    kill -9 $(cat ~/.jpypid)
}
spectrogram(){
	sox $1 -n spectrogram
	feh spectrogram.png	
	rm spectrogram.png
}

eval
	(cat ~/.cache/wal/sequences &)
	setopt HIST_IGNORE_SPACE

export LC_ALL=en_US.UTF-8

export PATH="/home/eshanrh/miniconda3/bin:$PATH"  # commented out by conda initialize  
export PATH="/home/eshanrh/.poetry/bin:$PATH"
export PATH="/home/eshanrh/.local/bin:$PATH"
export PATH="$PATH:/home/eshanrh/.gem/ruby/2.7.0/bin"

#osu envs and use poon's wine-osu patches
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export WINEPREFIX="$HOME/.wine_osu"
export WINEARCH=win32
export STAGING_AUDIO_DURATION=10000 
export PATH=/opt/wine-osu/bin:$PATH
