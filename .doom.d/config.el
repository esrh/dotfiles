;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Eshan Ramesh"
      user-mail-address "esrh@netc.eu")

(setq doom-font (font-spec :family "Hack" :size 14))

(setq doom-theme 'doom-gruvbox)

(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

(setq send-mail-function 'mailclient-send-it)

(setq display-line-numbers-type 'relative)

(global-set-key (kbd "S-RET") 'ein:worksheet-execute-cell-and-goto-next-km)

(setq +doom-dashboard-banner-file  "~/.doom.d/emacsgirl.png")
(setq +doom-dashboard-name "好きだよ")
(setq +doom-dashboard-functions '(doom-dashboard-widget-banner
                                 doom-dashboard-widget-shortmenu
                                 doom-dashboard-widget-loaded))
