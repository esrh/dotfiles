call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
call plug#end()
" au VimEnter * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Escape'
" au VimLeave * silent! !xmodmap -e 'clear Lock' -e 'keycode 0x42 = Caps_Lock'
set number
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
silent! nmap <F4> :NERDTreeToggle<CR>
