#!/usr/bin/env bash

artist=$(echo $(cmus-remote -Q | grep " artist " | cut -d " " -f3-))
title=$(echo $(cmus-remote -Q | grep "title" | cut -d " " -f3-))

echo -n "$artist - $title"
