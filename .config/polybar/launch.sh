# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config

if [ "$(xrandr --query | grep "Screen 0:" | cut -d " " -f8)" = 3840 ]; then
    echo "both"
    MONITOR=eDP polybar mybar &
    #MONITOR=HDMI-A-0 polybar mybar &
elif [ "$(xrandr --query | grep "HDMI-A-0" | cut -d " " -f2)" = "connected" ]; then
    echo "hdmi only"
    MONITOR=HDMI-A-0 polybar mybar &
else
    echo "only laptop"
    MONITOR=eDP polybar mybar &
fi
echo "Polybar launched..."
