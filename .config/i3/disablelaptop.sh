if [ "$(xrandr --query | grep "HDMI-A-0" | cut -d " " -f2)" = "connected ]; then
	xrandr --output HDMI-A-0 --primary
	xrandr --output eDP --off
